<?php
use Bitrix\Main\Localization\Loc;

$module_id = 'apf.blackhole'; //�����������, ����� ����� ������� �� ��������!

Loc::loadMessages(__FILE__);

/**
 * @var CMain
 */
global $APPLICATION;

if ($APPLICATION->GetGroupRight($module_id) < "R") {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

\Bitrix\Main\Loader::includeModule($module_id);
$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#�������� �����
$aTabs = array(
    array(
        'DIV'   => 'common',
        'TAB'   => GetMessage('APF_BLACKHOLE_TAB_SETTINGS'),
        'ICON'  => 'common',
        'TITLE' => GetMessage('APF_BLACKHOLE_TAB_SETTINGS')
    ),
    array(
        "DIV" => "rights",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),
);

#����������
if ($request->isPost() && $request['Update'] && check_bitrix_sessid())
{
    if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/modules/apf.blackhole/')) {
        include ($_SERVER['DOCUMENT_ROOT'] . '/local/modules/apf.blackhole/options/common-save.php');
    } else {
        include ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/apf.blackhole/options/common-save.php');
    }

}

#���������� �����
$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
    <form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='autoiblock_settings'>

        <?
        /* COMMON SETTINGS */
        $tabControl->BeginNextTab();
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/modules/apf.blackhole/')) {
            include ($_SERVER['DOCUMENT_ROOT'] . '/local/modules/apf.blackhole/options/common.php');
        } else {
            include ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/apf.blackhole/options/common.php');
        }
        $tabControl->EndTab();

        /* RIGHTS SETTINGS */
        $tabControl->BeginNextTab();
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
        $tabControl->EndTab();


        $tabControl->Buttons(); ?>

        <input type="submit" name="Update" value="<?echo GetMessage('MAIN_SAVE')?>">
        <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
        <?=bitrix_sessid_post();?>
    </form>
<? $tabControl->End(); ?>