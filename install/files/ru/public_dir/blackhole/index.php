<?
/** @global CMain $APPLICATION */
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC","Y");
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main;

if (Main\Loader::includeModule('apf.blackhole')) {

	$blackhole = new Apf\Blackhole\Blackhole();

	if ($blackhole->isWhiteList()) {
		LocalRedirect('/', false, '301 Moved Permanently');
	}

	$blackhole->addBot();

	$contacts_page = COption::GetOptionString('apf.blackhole',
		'APF_BLACKHOLE_CONTACTS_PAGE', '/contacts/');
	$recaptcha_key = COption::GetOptionString('apf.blackhole',
		'APF_BLACKHOLE_RECAPTCHA_SITE_KEY', '');
}
?>
<html>
<head>
	<title>Welcome to Blackhole!</title>
	<script src="//www.google.com/recaptcha/api.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<h1>Страница для робота</h1>
<p>
	Эта страница закрыта в файле <a target="_blank" href="/robots.txt">robots.txt</a>
	и на нее нет публичной ссылки.<br />
	Если вы попали на эту страницу по ссылкам на сайте - пожалуйста
	свяжитесь с <a href="<?=$contacts_page?>">администратором</a>.
</p>
<h3>Либо подтвердите, что вы не робот</h3>
<form id="whitelist_form" action="" method="post">
	<div class="g-recaptcha" data-sitekey="<?=$recaptcha_key?>"></div>
	<input type="submit" name="submit" value="Confirm">
</form>
</body>
</html>