<?php
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config as Conf;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

class apf_blackhole extends CModule {

    var $MODULE_ID  = 'apf.blackhole'; // требование маркетплейса!

    static $PUBLIC_FOLDER = '/';

    static $AGENT_CALCULATOR_PERIOD = 0;

    static $AGENT_CLEAR_PERIOD = 86400;

    protected $exclude_files;
    protected $events = array(
        array(
            'main',
            'OnPageStart',
            'apf.blackhole',
            '\Apf\Blackhole\Events\Main',
            'onPageStart_CheckRobot'
        ),

    );

    function __construct() {

        $arModuleVersion = array();
        include(__DIR__."/version.php");

        $this->exclude_files = array(
            '..',
            '.',
            'menu.php',
            'operation_description.php',
            'task_description.php'
        );

        $this->MODULE_ID = 'apf.blackhole';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("APF_BLACKHOLE_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("APF_BLACKHOLE_MODULE_DESC");

        $this->PARTNER_NAME = Loc::getMessage("APF_BLACKHOLE_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("APF_BLACKHOLE_PARTNER_URI");

        $this->MODULE_SORT = 1;
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS='Y';
        $this->MODULE_GROUP_RIGHTS = "Y";
    }

    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot = false)
    {
        if($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    function DoInstall()
    {
        global $APPLICATION;
        if ($this->isVersionD7()) {

            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();

            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
            // агенты должны ставится после установки модуля
            $this->InstallAgents();

        } else {
            $APPLICATION->ThrowException(Loc::getMessage("APF_BLACKHOLE_INSTALL_ERROR_VERSION"));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage("APF_BLACKHOLE_INSTALL_TITLE"), $this->GetPath() . "/install/step.php");
    }

    function DoUninstall()
    {
        global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

        if ($request["step"] < 2) {

            // $APPLICATION->IncludeAdminFile вызывает die()
            $APPLICATION->IncludeAdminFile(Loc::getMessage("APF_BLACKHOLE_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep1.php");

        } elseif ($request["step"] < 3) {

            $this->UnInstallFiles();
            $this->UnInstallEvents();
            $this->UnInstallAgents();

            if ($request["savedata"] != "Y") {
                $this->UnInstallDB();
            }

            Option::delete($this->MODULE_ID);

            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage("APF_BLACKHOLE_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep2.php");
        }
    }

    function InstallDB()
    {
        return true;
    }

    function UnInstallDB()
    {
        return true;
    }

    function InstallFiles($arParams = array())
    {
        $current_path = realpath(dirname(__FILE__));
        $dest_dir = $_SERVER['DOCUMENT_ROOT'] . '/local/';
        if(strpos($current_path, $dest_dir) === false) {
            $dest_dir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/';
        }

        $path = $this->GetPath() . "/install/components";

        if(\Bitrix\Main\IO\Directory::isDirectoryExists($path)) {
            CopyDirFiles($path, $dest_dir . "components", true, true);
        } else {
            //throw new \Bitrix\Main\IO\InvalidPathException($path);
        }

        if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath() . '/admin'))
        {
            CopyDirFiles($this->GetPath() . "/install/admin/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin"); //если есть файлы для копирования
            if ($dir = opendir($path))
            {
                while (false !== $item = readdir($dir))
                {
                    if (in_array($item,$this->exclude_files))
                        continue;
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $item,
                        '<'.'? require($_SERVER["DOCUMENT_ROOT"]."' . $this->GetPath(true) . '/admin/' . $item . '");?'.'>');
                }
                closedir($dir);
            }
        }

        // копируем фронт
        $path = $this->GetPath() . "/install/files/ru/public_dir";

        if (\Bitrix\Main\IO\Directory::isDirectoryExists($path)) {
            CopyDirFiles($path, $_SERVER["DOCUMENT_ROOT"] . self::$PUBLIC_FOLDER, true, true);
        } else {
            //throw new \Bitrix\Main\IO\InvalidPathException($path);
        }

        return true;
    }

    function UnInstallFiles()
    {
        $current_path = realpath(dirname(__FILE__));
        $dest_dir = $_SERVER['DOCUMENT_ROOT'] . '/local/';
        if(strpos($current_path, $dest_dir) === false) {
            $dest_dir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/';
        }

        \Bitrix\Main\IO\Directory::deleteDirectory($dest_dir . 'components/' . $this->MODULE_ID . '/');

        if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath() . '/admin')) {
            DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . $this->GetPath() . '/install/admin/', $_SERVER["DOCUMENT_ROOT"] . '/bitrix/admin');
            if ($dir = opendir($path)) {
                while (false !== $item = readdir($dir)) {
                    if (in_array($item, $this->exclude_files))
                        continue;
                    \Bitrix\Main\IO\File::deleteFile($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $item);
                }
                closedir($dir);
            }
        }
        return true;
    }

    function InstallEvents() {

        $eventManager = \Bitrix\Main\EventManager::getInstance();

        foreach ($this->events as $event) {
            $eventManager->registerEventHandler($event[0], $event[1], $event[2], $event[3], $event[4]);
        }
        return true;

    }

    function UnInstallEvents() {

        $eventManager = \Bitrix\Main\EventManager::getInstance();

        foreach ($this->events as $event) {
            $eventManager->unRegisterEventHandler($event[0], $event[1], $event[2], $event[3], $event[4]);
        }
        return true;
    }

    protected function InstallAgents() {

        return true;
    }

    protected function UnInstallAgents() {

        $agent = new CAgent();
        $agent->RemoveModuleAgents($this->MODULE_ID);
        return true;
    }
}