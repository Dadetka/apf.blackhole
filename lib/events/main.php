<?php
/**
 * Created by PhpStorm.
 * User: Ioannes
 * Date: 10.01.2017
 * Time: 23:40
 */
namespace Apf\Blackhole\Events;

use Apf\Blackhole\Blackhole;

class Main {

    public function onPageStart_CheckRobot() {

        $blackhole = new Blackhole();
        $blackhole->checkBot();
    }
}