<?php
/**
 * Created by PhpStorm.
 * User: Ioannes
 * Date: 30.09.2016
 * Time: 22:21
 */
namespace Apf\Blackhole;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Blackhole {

    private $black_list;
    private $white_list;

    private $user_ip;
    private $user_agent;
    private $user_protocol;
    private $user_method;
    private $user_request;

    private $data_file;

    private static $whois;

    private $email_to;
    private $recaptcha_site_key;
    private $recaptcha_secret_key;
    private $contacts_page;

    public function __construct() {

        global $BLACK_LIST, $WHITE_LIST;

        $file_path = $_SERVER['DOCUMENT_ROOT'] . '/upload/apf.blackhole/';

        if(!file_exists($file_path)) {
            mkdir($file_path, BX_DIR_PERMISSIONS, true);
        }

        $this->data_file = $file_path  . '/agents.php';
        if(file_exists($this->data_file)) {
            include_once $this->data_file;
        }

        $this->black_list = (array) $BLACK_LIST;
        $this->white_list = (array) $WHITE_LIST;

        $this->user_ip = get_ip();
        $this->user_agent = $this->sanitize($_SERVER['HTTP_USER_AGENT']);
        $this->user_request = $this->sanitize($_SERVER['REQUEST_URI']);
        $this->user_protocol = $this->sanitize($_SERVER['SERVER_PROTOCOL']);
        $this->user_method = $this->sanitize($_SERVER['REQUEST_METHOD']);

        $this->email_to = \COption::GetOptionString('apf.blackhole', 'APF_BLACKHOLE_EMAIL');
        $this->email_from = \COption::GetOptionString('main', 'email_from');
        $this->recaptcha_site_key = \COption::GetOptionString('apf.blackhole', 'APF_BLACKHOLE_RECAPTCHA_SITE_KEY');
        $this->recaptcha_secret_key = \COption::GetOptionString('apf.blackhole', 'APF_BLACKHOLE_RECAPTCHA_SECRET_KEY');
        $this->contacts_page = \COption::GetOptionString('apf.blackhole', 'APF_BLACKHOLE_CONTACTS_PAGE');
    }

    public function getIp() {
        return $this->user_ip;
    }

    public function whois() {

        if(self::$whois) {
            return self::$whois;
        }

        self::$whois = $this->shapeSpace_whois_lookup($this->user_ip);

        return self::$whois;
    }

    public function isWhiteList() {

        if($this->user_ip) {

            if (array_key_exists($this->user_ip, $this->white_list)) {
                return true;
            }
        }

        return false;
    }

    public function addBot() {

        if($this->user_ip) {

            if(array_key_exists($this->user_ip, $this->white_list)) {
                return true;
            }

            if(array_key_exists($this->user_ip, $this->black_list)) {
                return true;
            }

            // whitelist bots
            if (preg_match("/(bingbot|bingpreview|adsbot-google|googlebot|mediapartners-google|yandex)/i", $this->user_agent)) {
                while(ob_get_level()) {
                    ob_end_clean();
                }
                header('Location: /', true, 302);
                exit;
            }

            $user_ip = (string) $this->user_ip;
            $this->black_list[$user_ip] = array(
                'user_agent' => $this->user_agent,
                'protocol' => $this->user_protocol,
                'method' => $this->user_method,
                'date' => date('Y-m-d H:i:s'),
            );

            $this->saveData();

            while(ob_get_level()) {
                ob_end_clean();
            }

            $whois = $this->whois();

            $message   = date('d-m-Y H:i:s') . "\n\n";
            $message  .= 'URL Request: '. $this->user_request . "\n";
            $message  .= 'IP Address: '. (string) $this->user_ip . "\n";
            $message  .= 'User Agent: '. $this->user_agent . "\n\n";
            $message  .= 'Whois Lookup: '. "\n\n" . $whois . "\n";

            mail($this->email_to, 'Bad bot alert!', $message, 'From: '. $this->email_from);
        }
    }

    public function checkBot() {

        if($this->user_ip) {

            if(array_key_exists($this->user_ip, $this->white_list)) {
                return true;
            }

            if(array_key_exists($this->user_ip, $this->black_list)) {

                // ip banned
                if(isset($_REQUEST['g-recaptcha-response'])) {

                    $captcha = $_REQUEST['g-recaptcha-response'];

                    $raw_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" .
                        $this->recaptcha_secret_key .
                        "&response=" . $captcha .
                        "&remoteip=" . $_SERVER['REMOTE_ADDR']);

                    $response = json_decode($raw_response, true);

                    if($response['success']) {

                        $user_ip = (string) $this->user_ip;
                        unset($this->black_list[$user_ip]);
                        $this->white_list[$user_ip] = array(
                            'user_agent' => $this->user_agent,
                            'protocol' => $this->user_protocol,
                            'method' => $this->user_method,
                            'date' => date('Y-m-d H:i:s'),
                        );

                        $this->saveData();

                        LocalRedirect('/', false, '301 Moved Permanently');
                        die();
                    }
                }
                $content = '
<html>
  <head>
    <title>' . Loc::getMessage('APF_BLACKHOLE_IP_BLACKLISTED', array('#IP#' => $this->user_ip)) . '</title>
    <script src="//www.google.com/recaptcha/api.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body>
    <h1>' . Loc::getMessage('APF_BLACKHOLE_IP_BLACKLISTED', array('#IP#' => $this->user_ip)) . '</h1>
    <p>' . Loc::getMessage('APF_BLACKHOLE_CONFIRM_NOT_ROBOT') . '</p>
    <form id="whitelist_form" action="" method="post">
      <div class="g-recaptcha" data-sitekey="#RECAPTCHA_SECRET#"></div>
      <input type="submit" name="submit" value="Confirm">
    </form>
  </body>
</html>
';
                while(ob_get_level()) {
                    ob_end_clean();
                }
                echo str_replace('#RECAPTCHA_SECRET#', $this->recaptcha_site_key, $content);
                die();

            }

        } // if($this->user_ip)
    }

    private function saveData() {

        $data = '<?php
            global $BLACK_LIST, $WHITE_LIST;
            $BLACK_LIST = ' . var_export($this->black_list, true) . ';
            $WHITE_LIST = ' . var_export($this->white_list, true) . ';';

        $fp = fopen($this->data_file . '.tmp', 'w');

        if (flock($fp, LOCK_EX)) {  // выполняем эксклюзивную блокировку
            ftruncate($fp, 0);      // очищаем файл
            fwrite($fp, $data);
            fflush($fp);            // очищаем вывод перед отменой блокировки
            flock($fp, LOCK_UN);    // отпираем файл
            fclose($fp);
            $res = rename($this->data_file . '.tmp', $this->data_file);
            $debug = true;
        }
    }

    private function sanitize($string) {
        $string = trim($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
        $string = str_replace("\n", "", $string);
        $string = trim($string);
        return $string;
    }

    private function shapeSpace_whois_lookup($ipaddress) {

        $msg = '';
        $extra = '';
        $server = 'whois.arin.net';

        if (!$ipaddress = gethostbyname($ipaddress)) {

            $msg .= 'Can&rsquo;t perform lookup without an IP address.'. "\n\n";

        } else {

            if (!$sock = fsockopen($server, 43, $num, $error, 20)) {

                unset($sock);
                $msg .= 'Timed-out connecting to $server (port 43).'. "\n\n";

            } else {

                // fputs($sock, "$ipaddress\n");
                fputs($sock, "n $ipaddress\n");
                $buffer = '';
                while (!feof($sock)) $buffer .= fgets($sock, 10240);
                fclose($sock);

            }

            if (stripos($buffer, 'ripe.net')) {

                $nextServer = 'whois.ripe.net';

            } elseif (stripos($buffer, 'nic.ad.jp')) {

                $nextServer = 'whois.nic.ad.jp';
                $extra = '/e'; // suppress JaPaNIC characters

            } elseif (stripos($buffer, 'registro.br')) {

                $nextServer = 'whois.registro.br';

            }

            if (isset($nextServer)) {

                $buffer = '';
                $msg .= 'Deferred to specific whois server: '. $nextServer .'...'. "\n\n";

                if (!$sock = fsockopen($nextServer, 43, $num, $error, 10)) {

                    unset($sock);
                    $msg .= 'Timed-out connecting to '. $nextServer .' (port 43)'. "\n\n";

                } else {

                    fputs($sock, $ipaddress . $extra . "\n");
                    while (!feof($sock)) $buffer .= fgets($sock, 10240);
                    fclose($sock);

                }
            }

            $replacements = array("\n", "\n\n", "");
            $patterns = array("/\\n\\n\\n\\n/i", "/\\n\\n\\n/i", "/#(\s)?/i");
            $buffer = preg_replace($patterns, $replacements, $buffer);
            $buffer = htmlentities(trim($buffer), ENT_QUOTES, 'UTF-8');

            // $msg .= nl2br($buffer);
            $msg .= $buffer;

        }

        return $msg;
    }
}
