<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

return array(
    "APF_TASK_DENIED" => array(
        "title" => Loc::getMessage('APF_TASK_DENIED'),
        "description" => Loc::getMessage('APF_TASK_DENIED_DESC'),
    ),
    "APF_TASK_READ" => array(
        "title" => Loc::getMessage('APF_TASK_READ'),
        "description" => Loc::getMessage('APF_TASK_READ_DESC'),
    ),
    "APF_TASK_FULL_ACCESS" => array(
        "title" => Loc::getMessage('APF_TASK_FULL_ACCESS'),
        "description" => Loc::getMessage('APF_TASK_FULL_ACCESS_DESC'),
    ),
    "MODULE_TASK" => array(
        "title" => Loc::getMessage("TASK_BINDING_MODULE"),
    ),
);