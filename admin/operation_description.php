<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

return array(
    "APF_READ" => array(
        "title" => Loc::getMessage('APF_READ'),
        "description" => Loc::getMessage('APF_READ_DESC'),
    ),
    "APF_SETTINGS" => array(
        "title" => Loc::getMessage('APF_SETTINGS'),
        "description" => Loc::getMessage('APF_SETTINGS_DESC'),
    ),
    "APF_EDIT" => array(
        "title" => Loc::getMessage('APF_EDIT'),
        "description" => Loc::getMessage('APF_EDIT_DESC'),
    ),
);