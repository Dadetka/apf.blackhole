<?php
/**
 * ���������������� ���� � ����������� ������
 */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$email = COption::GetOptionString('main', 'email_from');

/*
 * ����� ��� �����������
 */
$APF_BLACKHOLE_EMAIL = COption::GetOptionString('apf.blackhole',
    'APF_BLACKHOLE_EMAIL', $email);
/*
 * reCAPTCHA Site Key
 */
$APF_BLACKHOLE_RECAPTCHA_SITE_KEY = COption::GetOptionString('apf.blackhole',
    'APF_BLACKHOLE_RECAPTCHA_SITE_KEY', '');
/*
 * reCAPTCHA Secret Key
 */
$APF_BLACKHOLE_RECAPTCHA_SECRET_KEY = COption::GetOptionString('apf.blackhole',
    'APF_BLACKHOLE_RECAPTCHA_SECRET_KEY', '');
/*
 * contacts page
 */
$APF_BLACKHOLE_CONTACTS_PAGE = COption::GetOptionString('apf.blackhole',
    'APF_BLACKHOLE_CONTACTS_PAGE', '/contacts/');
?>

<tr>
    <td width="50%">
        <label for="APF_BLACKHOLE_EMAIL">
            <?=Loc::getMessage("APF_BLACKHOLE_EMAIL_NOTIFICATION")?>:
        </label>
    </td>
    <td valign="top">
        <input
            type="text"
            size="50"
            name="APF_BLACKHOLE_EMAIL"
            id="APF_BLACKHOLE_EMAIL"
            value="<?=$APF_BLACKHOLE_EMAIL?>"
        />
    </td>
</tr>
<tr>
    <td width="50%">
        <label for="APF_BLACKHOLE_RECAPTCHA_SITE_KEY">
            reCAPTCHA Site key:
        </label>
    </td>
    <td valign="top">
        <input
            type="text"
            size="50"
            name="APF_BLACKHOLE_RECAPTCHA_SITE_KEY"
            id="APF_BLACKHOLE_RECAPTCHA_SITE_KEY"
            value="<?=$APF_BLACKHOLE_RECAPTCHA_SITE_KEY?>"
        />
    </td>
</tr>
<tr>
    <td width="50%">
        <label for="APF_BLACKHOLE_RECAPTCHA_SECRET_KEY">
            reCAPTCHA Secret key:
        </label>
    </td>
    <td valign="top">
        <input
            type="text"
            size="50"
            name="APF_BLACKHOLE_RECAPTCHA_SECRET_KEY"
            id="APF_BLACKHOLE_RECAPTCHA_SECRET_KEY"
            value="<?=$APF_BLACKHOLE_RECAPTCHA_SECRET_KEY?>"
        />
    </td>
</tr>
<tr>
    <td width="50%">
        <label for="APF_BLACKHOLE_CONTACTS_PAGE">
            <?=Loc::getMessage("APF_BLACKHOLE_CONTACTS_PAGE")?>:
        </label>
    </td>
    <td valign="top">
        <input
            type="text"
            size="50"
            name="APF_BLACKHOLE_CONTACTS_PAGE"
            id="APF_BLACKHOLE_CONTACTS_PAGE"
            value="<?=$APF_BLACKHOLE_CONTACTS_PAGE?>"
        />
    </td>
</tr>
<tr>
    <td colspan="2">
        <?= BeginNote();?>
        <?=Loc::getMessage("APF_BLACKHOLE_RECAPTCHA_NOTE");?>
        <?= EndNote(); ?>
    </td>
</tr>


