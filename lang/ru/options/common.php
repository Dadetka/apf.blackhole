<?php
/**
 * Created by PhpStorm.
 * User: Ioannes
 * Date: 09.01.2017
 * Time: 20:18
 */
$MESS["APF_BLACKHOLE_EMAIL_NOTIFICATION"] = "Основные настройки";
$MESS["APF_BLACKHOLE_CONTACTS_PAGE"] = "Страница с контактной информацией";
$MESS["APF_BLACKHOLE_RECAPTCHA_NOTE"] = "Получить ключи можно на <a target=\"_blank\" href=\"https://www.google.com/recaptcha/intro/index.html\">странице reCAPTCHA</a>";