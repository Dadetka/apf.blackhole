<?php
/**
 * Created by PhpStorm.
 * User: Ioannes
 * Date: 09.01.2017
 * Time: 9:54
 */
$MESS["APF_BLACKHOLE_IP_BLACKLISTED"] = "Ваш IP #IP# заблокирован.";
$MESS["APF_BLACKHOLE_IS_MISTAKE"] = "Если это произошло по ошибке";
$MESS["APF_BLACKHOLE_CONTACT_ADMINISTRATOR"] = "свяжитесь с администратором";
$MESS["APF_BLACKHOLE_CONFIRM_NOT_ROBOT"] = "Подтвердите что вы не робот, чтобы снять блокировку";

