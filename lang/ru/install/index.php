<?php
$MESS["APF_BLACKHOLE_MODULE_NAME"] = "Блокировщик роботов";
$MESS["APF_BLACKHOLE_MODULE_DESC"] = "Модуль позволяющий блокировать неопознанных роботов";
$MESS["APF_BLACKHOLE_PARTNER_NAME"] = "AutoPartFinder.info";
$MESS["APF_BLACKHOLE_PARTNER_URI"] = "https://autopartfinder.info/";

$MESS["APF_BLACKHOLE_INSTALL_TITLE"] = "Установка модуля";
$MESS["APF_BLACKHOLE_INSTALL_UNDER_CONSTRUCTION"] = "Модуль в разработке....";
$MESS["APF_BLACKHOLE_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";

$MESS["APF_BLACKHOLE_UNINSTALL_TITLE"] = "Удаление модуля";
